import { define } from 'typeorm-seeding';
import { faker } from '@faker-js/faker';
import { User } from '../entities/user.entity';

define(User, () => {
  const user = new User();
  user.firstName = faker.name.firstName();
  user.lastName = faker.name.lastName();
  user.age = parseInt(faker.random.numeric());
  return user;
});
