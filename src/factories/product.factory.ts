import { define } from 'typeorm-seeding';
import { faker } from '@faker-js/faker';
import { Product } from '../entities/product.entity';

define(Product, () => {
  const product = new Product();
  product.name = `${faker.random.word()} ${faker.random.word()}`;
  product.description = faker.lorem.text();
  product.price = parseInt(faker.random.numeric());
  product.stock = parseInt(faker.random.numeric());
  product.image = faker.image.fashion();
  return product;
});
