//   quantity: number;
//   product: Product;
//   order: Order;
import { define, factory } from 'typeorm-seeding';
import { OrderItem } from '../entities/order-item.entity';
import { faker } from '@faker-js/faker';
import { Product } from '../entities/product.entity';
import { Order } from '../entities/order.entity';

define(OrderItem, () => {
  const orderItem = new OrderItem();
  orderItem.quantity = parseInt(faker.random.numeric());
  orderItem.product = factory(Product)() as any;
  orderItem.order = factory(Order)() as any;
  return orderItem;
});
