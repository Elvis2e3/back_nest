import { define, factory } from 'typeorm-seeding';
import { Order } from '../entities/order.entity';
import { User } from '../entities/user.entity';

define(Order, () => {
  const order = new Order();
  order.user = factory(User)() as any;
  return order;
});
