import {
  IsString,
  IsNumber,
  IsNotEmpty,
  IsPositive,
  IsBoolean,
} from 'class-validator';
import { PartialType } from '@nestjs/mapped-types';

export class CreateUserDto {
  @IsString()
  @IsNotEmpty()
  readonly firstName: string;

  @IsString()
  @IsNotEmpty()
  readonly lastName: string;

  @IsNumber()
  @IsNotEmpty()
  @IsPositive()
  readonly age: number;

  @IsBoolean()
  readonly status: boolean;
}

export class UpdateUserDto extends PartialType(CreateUserDto) {}
