import { IsNotEmpty, IsBoolean } from 'class-validator';
import { PartialType } from '@nestjs/mapped-types';
import { User } from '../entities/user.entity';

export class CreateOrderDto {
  @IsNotEmpty()
  readonly user: User;

  @IsBoolean()
  readonly status: boolean;
}

export class UpdateOrderDto extends PartialType(CreateOrderDto) {}
