import {
  IsNumber,
  IsNotEmpty,
  IsPositive,
  IsBoolean,
} from 'class-validator';
import { PartialType } from '@nestjs/mapped-types';
import { Order } from '../entities/order.entity';
import { Product } from '../entities/product.entity';

export class CreateOrderItemDto {
  @IsNumber()
  @IsNotEmpty()
  @IsPositive()
  readonly quantity: number;

  @IsNotEmpty()
  readonly order: Order;

  @IsNotEmpty()
  readonly product: Product;

  @IsBoolean()
  readonly status: boolean;
}

export class UpdateOrderItemDto extends PartialType(CreateOrderItemDto) {}
