import { PrimaryGeneratedColumn, Column, Entity, OneToMany } from 'typeorm';
import { Order } from './order.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 255 })
  firstName: string;

  @Column({ type: 'varchar', length: 255 })
  lastName: string;

  @Column({ type: 'int' })
  age: number;

  @OneToMany(() => Order, (order) => order.user)
  orders: Order[];

  @Column({ type: 'boolean', default: true })
  status: boolean;
}
