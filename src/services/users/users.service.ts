import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../../entities/user.entity';
import { CreateUserDto, UpdateUserDto } from '../../dtos/users.tdos';

@Injectable()
export class UsersService {
  constructor(@InjectRepository(User) private userRepo: Repository<User>) {}
  findAll() {
    return this.userRepo.find();
  }
  async findOne(id: number) {
    const user = await this.userRepo.findOneBy({ id: id });
    if (!user) {
      throw new NotFoundException(`User ${id} not found`);
    }
    return user;
  }
  async create(payload: CreateUserDto) {
    const newUser = this.userRepo.create(payload);
    return await this.userRepo.save(newUser).catch((error) => {
      throw new NotFoundException(error.detail);
    });
  }
  async update(id: number, payload: UpdateUserDto) {
    const user = await this.findOne(id);
    this.userRepo.merge(user, payload);
    return this.userRepo.save(user).catch((error) => {
      throw new NotFoundException(error.detail);
    });
  }

  async delete(id: number) {
    const user = await this.findOne(id);
    return this.userRepo.delete(user.id).catch((error) => {
      throw new NotFoundException(error.detail);
    });
  }
}
