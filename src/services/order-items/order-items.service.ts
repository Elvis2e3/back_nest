import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { OrderItem } from '../../entities/order-item.entity';
import {
  CreateOrderItemDto,
  UpdateOrderItemDto,
} from '../../dtos/order-items.tdos';

@Injectable()
export class OrderItemsService {
  constructor(
    @InjectRepository(OrderItem) private orderItemRepo: Repository<OrderItem>,
  ) {}
  findAll() {
    return this.orderItemRepo.find({ relations: ['product', 'order'] });
  }
  async findOne(id: number) {
    const orderItem = await this.orderItemRepo.findOne({
      relations: ['product', 'order'],
      where: {
        id,
      },
    });
    if (!orderItem) {
      throw new NotFoundException(`OrderItem ${id} not found`);
    }
    return orderItem;
  }
  async create(payload: CreateOrderItemDto) {
    const newOrderItem = this.orderItemRepo.create(payload);
    return await this.orderItemRepo.save(newOrderItem).catch((error) => {
      throw new NotFoundException(error.detail);
    });
  }
  async update(id: number, payload: UpdateOrderItemDto) {
    const orderItem = await this.findOne(id);
    this.orderItemRepo.merge(orderItem, payload);
    return this.orderItemRepo.save(orderItem).catch((error) => {
      throw new NotFoundException(error.detail);
    });
  }

  async delete(id: number) {
    const orderItem = await this.findOne(id);
    return this.orderItemRepo.delete(orderItem.id).catch((error) => {
      throw new NotFoundException(error.detail);
    });;
  }
}
