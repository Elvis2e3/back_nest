import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Order } from '../../entities/order.entity';
import { CreateOrderDto, UpdateOrderDto } from '../../dtos/orders.tdos';

@Injectable()
export class OrdersService {
  constructor(@InjectRepository(Order) private orderRepo: Repository<Order>) {}
  findAll() {
    return this.orderRepo.find({ relations: ['user', 'items'] });
  }
  async findOne(id: number) {
    const order = await this.orderRepo.findOne({
      relations: ['user', 'items'],
      where: {
        id,
      },
    });
    if (!order) {
      throw new NotFoundException(`Order ${id} not found`);
    }
    return order;
  }
  async create(payload: CreateOrderDto) {
    const newOrder = this.orderRepo.create(payload);
    return await this.orderRepo.save(newOrder).catch((error) => {
      throw new NotFoundException(error.detail);
    });
  }
  async update(id: number, payload: UpdateOrderDto) {
    const order = await this.findOne(id);
    this.orderRepo.merge(order, payload);
    return this.orderRepo.save(order).catch((error) => {
      throw new NotFoundException(error.detail);
    });
  }

  async delete(id: number) {
    const order = await this.findOne(id);
    return this.orderRepo.delete(order.id).catch((error) => {
      throw new NotFoundException(error.detail);
    });
  }
}
