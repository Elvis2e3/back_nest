import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Product } from '../../entities/product.entity';
import { CreateProductDto, UpdateProductDto } from '../../dtos/products.tdos';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product) private productRepo: Repository<Product>,
  ) {}
  findAll() {
    return this.productRepo.find();
  }
  async findOne(id: number) {
    const product = await this.productRepo.findOneBy({ id: id });
    if (!product) {
      throw new NotFoundException(`Product ${id} not found`);
    }
    return product;
  }
  async create(payload: CreateProductDto) {
    const newProduct = this.productRepo.create(payload);
    return await this.productRepo.save(newProduct).catch((error) => {
      throw new NotFoundException(error.detail);
    });
  }
  async update(id: number, payload: UpdateProductDto) {
    const product = await this.findOne(id);
    this.productRepo.merge(product, payload);
    return this.productRepo.save(product).catch((error) => {
      throw new NotFoundException(error.detail);
    });
  }

  async delete(id: number) {
    const product = await this.findOne(id);
    return this.productRepo.delete(product.id).catch((error) => {
      throw new NotFoundException(error.detail);
    });
  }
}
