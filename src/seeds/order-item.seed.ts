import { Seeder, Factory } from 'typeorm-seeding';
import { Connection } from 'typeorm';
import { OrderItem } from '../entities/order-item.entity';

export default class CreateOrderItems implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    await factory(OrderItem)().createMany(20);
  }
}
