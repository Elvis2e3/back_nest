import { Seeder, Factory } from 'typeorm-seeding';
import { Connection } from 'typeorm';
import { Order } from '../entities/order.entity';

export default class CreateOrders implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    await factory(Order)().createMany(10);
  }
}
