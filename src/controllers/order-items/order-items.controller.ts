import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
} from '@nestjs/common';
import { OrderItemsService } from '../../services/order-items/order-items.service';
import {
  CreateOrderItemDto,
  UpdateOrderItemDto,
} from '../../dtos/order-items.tdos';

@Controller('order-items')
export class OrderItemsController {
  constructor(private orderItemService: OrderItemsService) {}
  @Get()
  list() {
    return this.orderItemService.findAll();
  }

  @Get(':id')
  getOne(@Param('id', ParseIntPipe) id: number) {
    return this.orderItemService.findOne(id);
  }

  @Post()
  create(@Body() payload: CreateOrderItemDto) {
    return this.orderItemService.create(payload);
  }

  @Put(':id')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() payload: UpdateOrderItemDto,
  ) {
    return this.orderItemService.update(id, payload);
  }

  @Delete(':id')
  delete(@Param('id', ParseIntPipe) id: number) {
    return this.orderItemService.delete(id);
  }
}
