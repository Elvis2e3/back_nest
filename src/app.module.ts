import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ProductsController } from './controllers/products/products.controller';
import { ProductsService } from './services/products/products.service';
import { OrdersController } from './controllers/orders/orders.controller';
import { OrdersService } from './services/orders/orders.service';
import { enviroments } from './enviroments';
import config from './config';
import { DatabaseModule } from './database/database.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Product } from './entities/product.entity';
import { User } from './entities/user.entity';
import { Order } from './entities/order.entity';
import { OrderItem } from './entities/order-item.entity';
import { OrderItemsService } from './services/order-items/order-items.service';
import { UsersService } from './services/users/users.service';
import { OrderItemsController } from './controllers/order-items/order-items.controller';
import { UsersController } from './controllers/users/users.controller';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: enviroments[process.env.NODE_ENV] || '.env',
      load: [config],
      isGlobal: true,
    }),
    DatabaseModule,
    TypeOrmModule.forFeature([Product, User, Order, OrderItem]),
  ],
  controllers: [
    AppController,
    ProductsController,
    OrdersController,
    OrderItemsController,
    UsersController,
  ],
  providers: [
    AppService,
    OrdersService,
    ProductsService,
    OrderItemsService,
    UsersService,
  ],
})
export class AppModule {}
