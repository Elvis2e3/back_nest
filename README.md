<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>

[circleci-image]: https://img.shields.io/circleci/build/github/nestjs/nest/master?token=abc123def456
[circleci-url]: https://circleci.com/gh/nestjs/nest

  <p align="center">A progressive <a href="http://nodejs.org" target="_blank">Node.js</a> framework for building efficient and scalable server-side applications.</p>
    <p align="center">

## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Start project
Config file `.env`: 
```javascript
POSTGRES_USER="postgres"
POSTGRES_PASSWORD="postgres"
POSTGRES_HOST="localhost"
POSTGRES_DB="back_2e3"
POSTGRES_PORT=5432

TYPEORM_CONNECTION=postgres
TYPEORM_SYNCHRONIZE=false
TYPEORM_LOGGING=true
TYPEORM_ENTITIES='src/**/*.entity.ts'
TYPEORM_MIGRATIONS='src/database/migration/*.ts'
TYPEORM_MIGRATIONS_TABLE_NAME=migrations
TYPEORM_SEEDING_FACTORIES='src/**/*.factory.{ts,js}'
TYPEORM_SEEDING_SEEDS='src/**/*.seed.{ts,js}'
TYPEORM_USERNAME="postgres"
TYPEORM_PASSWORD="postgres"
TYPEORM_HOST="localhost"
TYPEORM_DATABASE="back_2e3"
TYPEORM_PORT=5432

```

Migrations:
```bash
# generate migrations
$ npm run makemigrations

# run migrations
$ npm run migrate
```
Seeds:
```bash
# generate data
$ npm run seed:run
```

Postman:

Import file `post0822.postman_collection.json` in Postman
* Create User with endpoint `create user` => http://localhost/users/
* Create Product with endpoint `create product` => http://localhost/products/
* Create Order with endpoint `create order` => http://localhost/orders/
* Create OrderItem with endpoint `create orderItem` => http://localhost/order-items/
* More endpoints...

## Stay in touch

- Author - [Elvis Edson Basilio Chambi](https://www.linkedin.com/in/elvis-basilio-889659103)
- Website - [elvis2e3.com](https://www.elvis2e3.com/)
- Email - [elvis.2e3@gmail.com](elvis.2e3@gmail.com)

## License

Nest is [MIT licensed](LICENSE).
